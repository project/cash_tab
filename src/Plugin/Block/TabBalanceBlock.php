<?php

namespace Drupal\cash_tab\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\cash_tab\CashTab;

/**
 * Provides a 'TabBalanceBlock' block.
 *
 * @Block(
 *  id = "tab_balance_block",
 *  admin_label = @Translation("Cash Tab Balance Block"),
 * )
 */
class TabBalanceBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['label_display' => FALSE];
  }

  /**
   * Block constructor.
   *
   * @return array
   *   Render array of data to be passed to the theme layer.
   */
  public function build() {

    $current_user = \Drupal::currentUser();
    $uid = $current_user->id();
    $name = $current_user->getUsername();

    if($uid != 0) {
      $ct = new CashTab();
      $balance = $ct->getBalance($uid);

      return array(
        '#title' => 'Cash Tab Balance',
        '#description' => 'Block for displaying users current tab balance.',
        '#theme' => 'cash_tab_balance',
        '#variables' => array(
          'balance' => $balance,
          'name' => $name
        )
      );
    }
  }

  public function getCacheMaxAge() {
    return 0;
  }


}

