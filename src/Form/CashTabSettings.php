<?php
namespace Drupal\cash_tab\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * CashTabAddBalance class extending FormBase.
 */
class CashTabSettings extends FormBase {
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'cash_tab_settings';
  }

  public function buildForm (array $form, FormStateInterface $form_state) {
/*


              <input type="hidden" name="item_number" value="<?=$orderid;?>"> <!-- this can be the order ID -->
              <input type="hidden" name="invoice" value="<?=$orderid;?>"> <!-- this can be the order ID -->
              <input type="hidden" name="amount" value="<?=$_REQUEST['amount'];?>">
              <input type="hidden" name="no_shipping" value="2">
              <input type="hidden" name="no_note" value="1">
              <input type="hidden" name="return" value="http://login.selfserveleads.com/repository/selfserveleads.com/paypal.php"> <!-- get from config -->
              <input type="hidden" name="notify_url" value="http://reseller.leadpower.com/repository/selfserveleads.com/paypal_complete.php"> <!-- get from config -->
              <input type="hidden" name="rm" value="2"> <!-- get from config -->
              <input type="hidden" name="custom" value="<?=$_SESSION['username'];?>" /><!-- use to send userid-->
              <input type="hidden" name="currency_code" value="USD">
              <input type="hidden" name="bn" value="IC_Sample">
              <input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but23.gif" name="submit" alt="">
              <img alt="" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
            </form>
*/

// TODO: add currency from commerce settings, and shipping from product.

    // TODO: put in config
    $cmd_options = array(
      '_xclick' => '_xclick',
    );

    $paypal_endpoints = array(
      'https://www.paypal.com/cgi-bin/webscr' => 'LIVE',
    );

    $form['paypal_env'] = array (
      '#type' => 'select',
      '#title' => $this->t('Destination'),
      '#required' => TRUE,
      '#options' => $paypal_endpoints,
      '#description' => $this->t('PayPal environment(live, test etc)'),
      '#default_value' => \Drupal::state()->get('paypal_env'),
    );

    $form['paypal_cmd'] = array (
      '#type' => 'select',
      '#title' => $this->t('Payment amount'),
      '#required' => TRUE,
      '#options' => $cmd_options,
      '#description' => $this->t('CMD value'),
      '#default_value' => \Drupal::state()->get('paypal_cmd', '_xclick'),
    );

    $form['paypal_owner_email'] = array (
      '#type' => 'textfield',
      '#title' => $this->t('Account Email'),
      '#required' => TRUE,
      '#description' => $this->t('Paypal account holder email'),
      '#default_value' => \Drupal::state()->get('paypal_owner_email'),
    );

    $form['paypal_item_name'] = array (
      '#type' => 'textfield',
      '#title' => $this->t('Item Name'),
      '#required' => TRUE,
      '#description' => $this->t('Name of product that shows in the purchase window of PayPal'),
      '#default_value' => \Drupal::state()->get('paypal_item_name'),
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add Money'),
    );

    return $form;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

}
