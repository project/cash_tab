<?php
namespace Drupal\cash_tab\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * CashTabAddBalance class extending FormBase.
 */
class CashTabAddBalance extends FormBase {
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'cash_tab_add_balance';
  }

  public function buildForm (array $form, FormStateInterface $form_state) {

    // TODO: put in config
    $payment_options = array(
      '20.00' => '$20.00',
      '25.00' => '$25.00',
      '100.00' => '$100.00',
      '250.00' => '$250.00',
      '500.00' => '$500.00',
      '1000.00' => '$1000.00',
    );

    $form['balance_amount'] = array (
      '#type' => 'select',
      '#title' => $this->t('Payment amount'),
      '#required' => TRUE,
      '#options' => $payment_options,
      '#description' => $this->t('Select how much money you want to add to yourself serve leads account.'),
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add Money'),
    );

    return $form;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

}
