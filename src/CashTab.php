<?php
namespace Drupal\cash_tab;

/**
 * Class CashTab
 */
class CashTab {


  public function updateBalance() {

  }

  /**
   * @param $uid
   * @return string
   */
  public function getBalance($uid) {

    $query = \Drupal::database()->select('cash_tab_balances', 'ctb');
    $query-> addField ('ctb', 'balance');
    $query->condition('ctb.uid', $uid);
    $balance_record = $query->execute()->fetchField();

    if($balance_record == NULL) {
      \Drupal::database()->insert('cash_tab_balances')
        ->fields([
          'uid' => $uid,
          'balance' => 0,
        ])
        ->execute();
      return '0';
    }
// TODO: make this a setting
    setlocale(LC_MONETARY, 'en_US');
   $balance = money_format('%.2n', $balance_record);
   // dpm($balance);
   return $balance;
  }

  /**
   *
   */
  public function registerUser() {

  }

  /**
   *
   */
  public function checkUser() {

  }


}
